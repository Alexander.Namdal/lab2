package INF101.lab2.pokemon;
import java.util.Random;

public class Pokemon {


    private final String name;
    private final int strength;
    private int healthPoints;
    private final int maxHealthPoints;
    private final Random random = new Random();

    public Pokemon(String name, int healthPoints, int strength) {

        this.name = name;
        this.healthPoints = healthPoints;
        this.maxHealthPoints = healthPoints;
        this.strength = strength;
    }


    public String getName() {return this.name;}

    public int getStrength(){return this.strength;}

    public int getCurrentHP() {return this.healthPoints;}

    public int getMaxHP() {return this.maxHealthPoints;}

    public boolean isAlive() {return (this.healthPoints > 0);}



    public void damage(int damageTaken) {
        if(damageTaken > 0)
            this.healthPoints -= damageTaken;
        if(this.healthPoints < 0)
            this.healthPoints = 0;
        System.out.printf("%s takes %d damage and is left with %d/%d HP\n",name, damageTaken, healthPoints, maxHealthPoints);
    }

    public void attack(Pokemon target) {
        int damageInflicted = (int) (this.strength + this.strength / 2 * random.nextGaussian());
        System.out.printf("%s attacks %s\n",this.name, target.getName());
        target.damage(damageInflicted);
        if (!target.isAlive())
            System.out.printf("%s is defeated by %s.", target.getName(), this.name);
    }

    @Override
    public String toString() {
        return String.format("%s HP: (%d/%d) STR: %d",name, healthPoints, maxHealthPoints, strength);
    }

}
