package INF101.lab2.pokemon;

public class Main {

    public static Pokemon pokemon1;
    public static Pokemon pokemon2;
    public static void main(String[] args) {
        pokemon1 = new Pokemon("Truls", 300, 30);
        pokemon2 = new Pokemon("Oddleif", 300, 30);
        System.out.println(pokemon1);
        System.out.println(pokemon2+"\n");

        while (true){
            pokemon1.attack(pokemon2);
            if(!pokemon2.isAlive()){
                break;
            }
            try {
                Thread.sleep(1000); // Delay for 1 second
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            pokemon2.attack(pokemon1);
            if(!pokemon1.isAlive()){
                break;
            }

            try {
                Thread.sleep(1000); // Delay for 1 second
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}
